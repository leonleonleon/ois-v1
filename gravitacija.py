v = float(input("Visina:"))*1000
grav = ((6.674 * 10**-11)*(5.972 * 10**24))/((6.371 * 10**6 + v)**2)
print("Gravitaijski pospešek na %.1f km je enak %.2f m/s^2" % (v/1000, grav))